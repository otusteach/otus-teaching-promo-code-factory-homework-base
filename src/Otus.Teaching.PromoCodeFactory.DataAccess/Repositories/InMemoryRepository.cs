﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task CreateAsync(T in_t)
        {
            await Task.Run(() => Data = Data.Append(in_t));
        }

        public async Task DeleteAsync(Guid in_id)
        {
            await Task.Run(() => Data = Data.Where(x => x.Id != in_id));
        }

        public async Task UpdateAsync(T in_t)
        {
           await Task.Run(() => Data = Data.Where(x => x.Id != in_t.Id).Append(in_t));
        }
    }
}