﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary> Добавить сотрудника </summary>
        [HttpPost]
        public async Task CreateEmployee(Employee in_emp)
        {
            await _employeeRepository.CreateAsync(in_emp);
        }

        /// <summary> Обновить данные о сотруднике </summary>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateEmployee(Guid id, Employee in_employee)
        {
            try
            {
                var empl = _employeeRepository.GetByIdAsync(id);

                if (in_employee == null)
                    return NotFound($"Не найден сотрудник по идентификатору: {id}");

                await _employeeRepository.UpdateAsync(in_employee);
                return Ok("success");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
            
        }

        /// <summary> Удалать сотрудника по Id </summary>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            try
            {
                var empl = _employeeRepository.GetByIdAsync(id);

                if (empl == null)
                    return NotFound($"Не найден сотрудник по идентификатору: {id}");

                await _employeeRepository.DeleteAsync(id);

                return Ok("success");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}